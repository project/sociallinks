<div id="sociallinks">
  <ul class="sociallinks-tabs clear-block">
    <?php print drupal_render($tabs); ?>
  </ul>
  <?php print drupal_render($content); ?>
  <div class="clear-block"></div>
</div>
